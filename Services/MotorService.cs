﻿using Microsoft.EntityFrameworkCore;
using Quiz1_Yoga.Entities;
using Quiz1_Yoga.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quiz1_Yoga.Services
{
    public class MotorService
    {
        private readonly ManagementDbContext _db;

        /// <summary>
        /// Dependency injection ManagementDbContext
        /// </summary>
        /// <param name="dbContext"></param>
        public MotorService(ManagementDbContext dbContext)
        {
            this._db = dbContext;
        }

        /// <summary>
        /// Method menampilkan semua motor
        /// </summary>
        /// <returns></returns>
        public async Task<List<MotorModel>> GetAllMotorsAsync()
        {
            var motors = await this._db
                .Motors
                .Select(Q => new MotorModel
                {
                    MotorId = Q.MotorId,
                    MotorName = Q.MotorName,
                    JumlahMotor = Q.JumlahMotor
                })
                .AsNoTracking()
                .ToListAsync();

            return motors;
        }

        /// <summary>
        /// Method menambahkan motor baru
        /// </summary>
        /// <param name="motorModel"></param>
        /// <returns></returns>
        public async Task<bool> InsertNewMotorAsync(int id, string name, int jumlah)
        {
            this._db.Add(new Motor
            {
                MotorId = id,
                MotorName = name,
                JumlahMotor = jumlah
            });

            await this._db.SaveChangesAsync();

            return true;
        }

        /// <summary>
        /// Method untuk ambil spesifik motor sebelum diupdate
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<MotorModel> GetSpecificMotorAsync(int? id)
        {
            var motors = await this._db
                .Motors
                .Where(Q => Q.MotorId == id)
                .Select(Q => new MotorModel
                {
                    MotorId = Q.MotorId,
                    MotorName = Q.MotorName,
                    JumlahMotor = Q.JumlahMotor
                })
                .FirstOrDefaultAsync();
            return motors;
        }

        /// <summary>
        /// Method untuk update motor
        /// </summary>
        /// <param name="motorModel"></param>
        /// <returns></returns>
        public async Task<bool> UpdateMotorAsync(int id, string name, int jumlah)
        {
            var motor = await this._db
                .Motors
                .Where(Q => Q.MotorId == id)
                .FirstOrDefaultAsync();

            if (motor == null)
            {
                return false;
            }

            motor.MotorName = name;
            motor.JumlahMotor = jumlah;

            await this._db.SaveChangesAsync();
            return true;
        }

        /// <summary>
        /// Method untuk delete motor
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<bool> DeleteMotorAsync(int id)
        {
            var motor = await this._db
                .Motors
                .Where(Q => Q.MotorId == id)
                .FirstOrDefaultAsync();

            if (motor == null)
            {
                return false;
            }

            this._db.Remove(motor);

            await this._db.SaveChangesAsync();

            return true;
        }

        /// <summary>
        /// method dapetin jumlah semua motor
        /// </summary>
        /// <returns></returns>
        public int GetTotalData()
        {
            var totalMotor = this._db
                .Motors
                .Count();

            return totalMotor;
        }

        /// <summary>
        /// method untuk filtering
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="itemPerPage"></param>
        /// <param name="filterByName"></param>
        /// <returns></returns>
        public async Task<List<MotorModel>> GetAsync(int pageIndex, int itemPerPage, string filterByName)
        {
            var query = this._db
                .Motors
                .AsQueryable();

            // for filter
            if (string.IsNullOrEmpty(filterByName) == false)
            {
                query = query
                    .Where(Q => Q.MotorName.StartsWith(filterByName));
            }

            // get data after filtering
            var motors = await query
                .Select(Q => new MotorModel
                {
                    MotorId = Q.MotorId,
                    MotorName = Q.MotorName,
                    JumlahMotor = Q.JumlahMotor
                })
                .Skip((pageIndex - 1) * itemPerPage)
                .Take(itemPerPage)
                .AsNoTracking()
                .ToListAsync();

            return motors;
        }
    }
}
