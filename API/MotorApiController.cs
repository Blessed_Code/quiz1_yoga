﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Quiz1_Yoga.Models;
using Quiz1_Yoga.Services;

namespace Quiz1_Yoga.API
{
    [Route("api/v1/motor")]
    [ApiController]
    public class MotorApiController : ControllerBase
    {
        private readonly MotorService _serviceMan;

        /// <summary>
        /// dependency injection
        /// </summary>
        /// <param name="motorService"></param>
        public MotorApiController(MotorService motorService)
        {
            this._serviceMan = motorService;
        }


        /// <summary>
        /// API untuk menampilkan semua data motor
        /// </summary>
        /// <returns></returns>
        [HttpGet("all-motor", Name = "getAllMotor")]
        public async Task<ActionResult<List<MotorModel>>> GetAllMotorAsync()
        {
            var motors = await _serviceMan.GetAllMotorsAsync();
            return Ok(motors);
        }

        /// <summary>
        /// API untuk insert data motor baru
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        [HttpPost("insert-motor", Name = "insertNewMotor")]
        public async Task<ActionResult<ResponseModel>> InsertNewMotorAsync(int id,[FromQuery]string name, [FromQuery]int jumlah)
        {
            var isSuccess = await _serviceMan.InsertNewMotorAsync(id, name, jumlah);

            if (isSuccess == false)
            {
                return BadRequest(new ResponseModel 
                {
                    ResponseMessage = "Id not found"
                });
            }

            return Ok(new ResponseModel 
            {
                ResponseMessage = "Success insert new data"
            });
        }


        /// <summary>
        /// API untuk ambil motor untuk di update
        /// </summary>
        /// <param name="motorId"></param>
        /// <returns></returns>
        [HttpGet("get-specific-motor", Name = "getSpecificMotor")]
        public async Task<ActionResult<MotorModel>> GetSpecificMotorAsync(int? motorId)
        {
            if (motorId.HasValue == false)
            {
                return BadRequest(null);
            }

            var motor = await _serviceMan.GetSpecificMotorAsync(motorId.Value);

            if (motor == null)
            {
                return BadRequest(null);
            }
            return Ok(motor);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        [HttpPut("update-motor", Name = "updateMotor")]
        public async Task<ActionResult<ResponseModel>> UpdateMotorAsync(int id, [FromQuery]string name, [FromQuery]int jumlah)
        {
            var isSuccess = await _serviceMan.UpdateMotorAsync(id, name, jumlah);
            
            if (isSuccess == false)
            {
                return BadRequest(new ResponseModel 
                {
                    ResponseMessage = "Id not found"
                });
            }

            return Ok(new ResponseModel 
            {
                ResponseMessage = "Success update data"
            });
        }

        [HttpDelete("delete-motor/{motorId}", Name = "deleteMotor")]
        public async Task<ActionResult<ResponseModel>> DeleteEmployeeAsync(int motorId)
        {
            var isSuccess = await _serviceMan.DeleteMotorAsync(motorId);
            
            if (isSuccess == false)
            {
                return BadRequest(new ResponseModel 
                {
                    ResponseMessage = "Id not found"
                });
            }

            return Ok(new ResponseModel 
            {
                ResponseMessage = "Success delete employee"
            });
        }

        [HttpGet("filter-data", Name = "getFilterData")]
        public async Task<ActionResult<List<MotorModel>>> GetFilterDataAsync([FromQuery] int pageIndex, int itemPerPage, string filterByName)
        {
            var data = await _serviceMan.GetAsync(pageIndex, itemPerPage, filterByName);
            return Ok(data);
        }

        [HttpGet("total-data", Name = "getTotalData")]
        public ActionResult<int> GetTotalData()
        {
            var totalData = _serviceMan.GetTotalData();
            return Ok(totalData);
        }
    }
}