﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quiz1_Yoga.Models
{
    public class ResponseModel
    {
        /// <summary>
        /// Merupakan model untuk mereturn status message
        /// </summary>
        public string ResponseMessage { get; set; }

        public string Status { get; set; }
    }
}
