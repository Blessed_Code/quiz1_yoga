﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Quiz1_Yoga.Models
{
    public class MotorModel
    {
        /// <summary>
        /// Merupakan model Motor
        /// </summary>
        public int MotorId { get; set; }

        [Required]
        [StringLength(7, MinimumLength = 3)]
        public string MotorName { get; set; }

        [Required]
        public int JumlahMotor { get; set; }
    }
}
